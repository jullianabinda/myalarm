package com.example.alarmoficial

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import java.util.Locale

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button: Button = findViewById(R.id.button)
        val editTextHours: EditText = findViewById(R.id.editTextHours)
        val editTextMinutes: EditText = findViewById(R.id.editTextMinutes)

        button.setOnClickListener{
            try{

            val hours = editTextHours.text.toString().toInt()
            val minutes = editTextMinutes.text.toString().toInt()


            val currentTime = Calendar.getInstance()
            val targetTime = Calendar.getInstance()
            targetTime.set(Calendar.HOUR_OF_DAY, hours)
            targetTime.set(Calendar.MINUTE, minutes)
            targetTime.set(Calendar.SECOND, 0)

            val timeDifference = targetTime.timeInMillis - currentTime.timeInMillis

            if (timeDifference <= 0) {
                // Se o tempo selecionado já passou hoje, adicionar um dia para tocar no mesmo horário amanhã
                targetTime.add(Calendar.DAY_OF_YEAR, 1)
            }

                val alarmIntent = Intent(applicationContext, MyBroadcastReceiver::class.java)
                val pendingIntent = PendingIntent.getBroadcast(applicationContext, 0, alarmIntent, PendingIntent.FLAG_IMMUTABLE)
            val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.set(AlarmManager.RTC_WAKEUP, targetTime.timeInMillis, pendingIntent)

            val formattedTime = SimpleDateFormat("HH:mm", Locale.getDefault()).format(targetTime.time)
            Toast.makeText(applicationContext, "O alarme irá tocar às $formattedTime.", Toast.LENGTH_LONG).show()
        } catch (e: NumberFormatException) {
                Toast.makeText(applicationContext, "Por favor, insira um valor numérico válido para horas e minutos.", Toast.LENGTH_SHORT).show()
            }        }
    }
}